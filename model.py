#!/usr/bin/env python3

import tensorflow as tf
from os import path

if __name__ == "__main__":
    # Define the model
    class CcsModel(tf.keras.Model):
        def __init__(self, **kwargs):
            super(CcsModel, self).__init__(**kwargs)
            self.s1_conv1 = tf.keras.layers.Conv2D(512, (3, 3))
            self.s1_relu1 = tf.keras.layers.ReLU()
            self.s1_conv2 = tf.keras.layers.Conv2D(256, (3, 3), padding='same')
            self.s1_relu2 = tf.keras.layers.ReLU()
            self.s1_conv2 = tf.keras.layers.Conv2D(64, (3, 3), padding='same')
            self.s1_relu2 = tf.keras.layers.ReLU()
            self.s1_pool1 = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))

            self.s2_conv1 = tf.keras.layers.Conv2D(64, (1, 1))
            self.s2_relu1 = tf.keras.layers.ReLU()
            self.s2_conv2 = tf.keras.layers.Conv2D(64, (3, 3), padding='same')
            self.s2_relu2 = tf.keras.layers.ReLU()
            self.s2_conv3 = tf.keras.layers.Conv2D(64, (1, 1))
            self.s2_add1 = tf.keras.layers.Add()
            self.s2_relu3 = tf.keras.layers.ReLU()

            self.s3_conv1 = tf.keras.layers.Conv2D(64, (1, 1))
            self.s3_relu1 = tf.keras.layers.ReLU()
            self.s3_conv2 = tf.keras.layers.Conv2D(64, (3, 3), padding='same')
            self.s3_relu2 = tf.keras.layers.ReLU()
            self.s3_conv3 = tf.keras.layers.Conv2D(64, (1, 1))
            self.s3_add1 = tf.keras.layers.Add()
            self.s3_relu3 = tf.keras.layers.ReLU()

            self.s4_conv1 = tf.keras.layers.Conv2D(64, (1, 1))
            self.s4_relu1 = tf.keras.layers.ReLU()
            self.s4_conv2 = tf.keras.layers.Conv2D(64, (3, 3), padding='same')
            self.s4_relu2 = tf.keras.layers.ReLU()
            self.s4_conv3 = tf.keras.layers.Conv2D(64, (1, 1))
            self.s4_add1 = tf.keras.layers.Add()
            self.s4_relu3 = tf.keras.layers.ReLU()

            self.flatten1 = tf.keras.layers.Flatten()

            self.dense1 = tf.keras.layers.Dense(128, activation=tf.keras.activations.relu)
            self.dense2 = tf.keras.layers.Dense(128, activation=tf.keras.activations.relu)
            self.dense3 = tf.keras.layers.Dense(128, activation=tf.keras.activations.relu)

            self.policy1 = tf.keras.layers.Dense(3584, activation=tf.keras.activations.softmax)
            self.value1 = tf.keras.layers.Dense(1, activation=tf.keras.activations.sigmoid)
            self.scores1 = tf.keras.layers.Dense(2, activation=tf.keras.activations.relu)

            self.checkpoint = tf.train.Checkpoint(self)
            self.optimizer = tf.keras.optimizers.RMSprop(learning_rate=2e-5, momentum=0.9)

        def call(self, inputs):
            state, drawn, scores = inputs
            return self.run(state, drawn, scores)

        @tf.function
        def set_learning_rate(self, rate):
            self.optimizer.learning_rate.assign(rate)
            return tf.constant(1)

        @tf.function
        def run(self, state, drawn, cur_scores):
            state = tf.reshape(state, [-1, 16, 16, 110])
            drawn = tf.reshape(drawn, [-1, 24])
            cur_scores = tf.reshape(cur_scores, [-1, 2])

            x = self.s1_conv1(state)
            x = self.s1_relu1(x)
            x = self.s1_conv2(x)
            x = self.s1_relu2(x)
            x = self.s1_pool1(x)

            x_s = x
            x = self.s2_conv1(x)
            x = self.s2_relu1(x)
            x = self.s2_conv2(x)
            x = self.s2_relu2(x)
            x = self.s2_conv3(x)
            x = self.s2_add1([x, x_s])
            x = self.s2_relu3(x)

            x_s = x
            x = self.s3_conv1(x)
            x = self.s3_relu1(x)
            x = self.s3_conv2(x)
            x = self.s3_relu2(x)
            x = self.s3_conv3(x)
            x = self.s3_add1([x, x_s])
            x = self.s3_relu3(x)

            x_s = x
            x = self.s4_conv1(x)
            x = self.s4_relu1(x)
            x = self.s4_conv2(x)
            x = self.s4_relu2(x)
            x = self.s4_conv3(x)
            x = self.s4_add1([x, x_s])
            x = self.s4_relu3(x)

            x = self.flatten1(x)
            x = tf.keras.layers.concatenate([x, drawn, cur_scores])

            x = self.dense1(x)
            x = self.dense2(x)
            x = self.dense3(x)
            policy = self.policy1(x)
            value = self.value1(x)
            scores = self.scores1(x)

            policy = tf.reshape(policy, [-1, 3584])
            scores = tf.reshape(scores, [-1, 2])
            return (policy, value, scores)

        @tf.function
        def train(self, state, drawn, cur_scores, policy_t, value_t, scores_t):
            policy_t = tf.reshape(policy_t, [3584, -1])
            with tf.GradientTape() as tape:
                policy, value, scores = self.run(state, drawn, cur_scores)
                value_term = (value - value_t) ** 2
                policy = tf.clip_by_value(policy, clip_value_min=0.0000001, clip_value_max=1.0)
                policy_term = tf.matmul(tf.math.log(policy), policy_t)
                scores_term = tf.keras.losses.MSE(scores_t, scores)
                loss_value = value_term - policy_term + scores_term * 0.2
            grads = tape.gradient(loss_value, model.trainable_variables)
            self.optimizer.apply_gradients(zip(grads, model.trainable_variables))
            return loss_value[0][0], value_term[0][0], policy_term, scores_term

    model = CcsModel()
    _x = tf.random.normal(shape=(16, 16, 110), dtype=tf.float32)
    _y = tf.random.normal(shape=(24, ), dtype=tf.float32)
    _z = tf.random.normal(shape=(2, ), dtype=tf.float32)
    _ = model((_x, _y, _z))
    model.summary()

    # Save the model
    scriptpath = path.dirname(path.realpath(__file__))
    modelpath = path.join(scriptpath, 'baseline_model')

    print('Saving model...')
    tf.saved_model.save(model, modelpath, signatures={
        'predict': model.run.get_concrete_function(
            tf.TensorSpec(shape=[16, 16, 110], dtype=tf.float32, name='state'),
            tf.TensorSpec(shape=[24], dtype=tf.float32, name='drawn'),
            tf.TensorSpec(shape=[2], dtype=tf.float32, name='cur_scores'),
        ),
        'train': model.train.get_concrete_function(
            tf.TensorSpec(shape=[16, 16, 110], dtype=tf.float32, name='state'),
            tf.TensorSpec(shape=[24], dtype=tf.float32, name='drawn'),
            tf.TensorSpec(shape=[2], dtype=tf.float32, name='cur_scores'),
            tf.TensorSpec(shape=[3584], dtype=tf.float32, name='policy_t'),
            tf.TensorSpec(shape=[], dtype=tf.float32, name='value_t'),
            tf.TensorSpec(shape=[2], dtype=tf.float32, name='scores_t'),
        ),
        'set_learning_rate': model.set_learning_rate.get_concrete_function(
            tf.TensorSpec(shape=[], dtype=tf.float32, name='rate')
        ),
    })
