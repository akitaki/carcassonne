//! Adapted from work by Eike Schulze <eike.schulze@web.de>, licensed under the Unlicense License.
//! Repository is at <https://github.com/EikeSchulze/image-viewer-rust.git>.

use image::DynamicImage;

pub trait AsImg {
    /// Get a bytes data of the image. This is not intended to be used outside of the trait
    /// implementer itself.
    fn img_bytes(&self) -> &'static [u8] {
        panic!("Neither `img_bytes` nor `dyn_img` are implemented");
    }

    fn dyn_img(&self) -> Result<DynamicImage, Box<dyn std::error::Error>> {
        let dyn_img = image::io::Reader::with_format(
            std::io::Cursor::new(self.img_bytes()),
            image::ImageFormat::Png,
        )
        .decode()?;
        Ok(dyn_img)
    }

    fn save_img<T: AsRef<str>>(&self, path: T) -> Result<(), Box<dyn std::error::Error>> {
        let path = path.as_ref();
        let path = std::path::Path::new(&path);
        let img = self.dyn_img()?;
        if let Err(e) = img.save(path) {
            eprintln!("Warning: failed to save image: {:?}", e);
        }
        Ok(())
    }
}
