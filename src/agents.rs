use crate::algo::{MCTS, NNMCTS};
use crate::ccs;
use crate::nn_model::Model;
use tensorflow as tf;

pub trait Agent {
    type S;
    type A;

    fn query_action(&mut self, state: &Self::S) -> Self::A;
}

pub struct RandAgent;

impl Agent for RandAgent {
    type S = ccs::State;

    type A = ccs::Action;

    fn query_action(&mut self, state: &Self::S) -> Self::A {
        use rand::prelude::*;
        let mut state = state.clone();
        state.get_derivatives().choose(&mut thread_rng()).unwrap().1
    }
}

pub struct MCTSAgent;

impl MCTS for MCTSAgent {
    type S = ccs::State;

    type A = ccs::Action;

    type P = ccs::Player;

    type SI = std::vec::IntoIter<(ccs::State, ccs::Action)>;

    fn get_next_player_from_state(state: Self::S) -> Self::P {
        state.get_next_player()
    }

    fn get_winner_from_state(state: Self::S) -> Option<Self::P> {
        state.get_winner()
    }

    fn get_is_ended_from_state(state: &Self::S) -> bool {
        state.is_ended()
    }

    fn get_derivatives_from_state(state: &Self::S) -> Self::SI {
        let start = std::time::Instant::now();
        let mut state = state.clone();
        let res = state.get_derivatives();
        let end = std::time::Instant::now();
        res
    }

    fn get_random_derivative_from_state(state: &Self::S) -> Option<Self::S> {
        use rand::prelude::*;
        let mut state = state.clone();
        let action = state
            .draw_and_get_legal_actions()
            .choose(&mut thread_rng())
            .cloned();
        if let Some(action) = action {
            Some(state.get_state_after_action(action))
        } else {
            None
        }
    }

    fn get_last_action_from_state(state: &Self::S) -> Option<Self::A> {
        state.get_last_action()
    }

    fn get_result(state: &Self::S, action: &Self::A) -> Self::S {
        state.get_state_after_action(*action)
    }
}

impl Agent for MCTSAgent {
    type S = ccs::State;

    type A = ccs::Action;

    fn query_action(&mut self, state: &Self::S) -> Self::A {
        self.query_mcts(state)
    }
}

pub struct NNMCTSAgent {
    model: Model,
    playout_cap: usize,
}

impl NNMCTSAgent {
    pub fn new<S: AsRef<str>>(saved_model_path: S) -> Result<Self, Box<dyn std::error::Error>> {
        Ok(Self {
            model: Model::new_from_saved_model_path(saved_model_path)?,
            playout_cap: 500,
        })
    }

    pub fn set_playout_cap(&mut self, playout_cap: usize) {
        self.playout_cap = playout_cap;
    }

    pub fn save_model<S: AsRef<str>>(&mut self, to_path: S) {
        self.model
            .save_model_to_path(to_path.as_ref())
            .expect(&format!(
                "Failed to save model to path {}",
                to_path.as_ref()
            ));
    }

    pub fn get_model(&mut self) -> &mut Model {
        &mut self.model
    }
}

impl NNMCTS for NNMCTSAgent {
    type S = ccs::State;

    type A = ccs::Action;

    type P = ccs::Player;

    type SI = std::vec::IntoIter<(ccs::State, ccs::Action)>;

    fn get_next_player_from_state(state: Self::S) -> Self::P {
        state.get_next_player()
    }

    fn get_winner_from_state(state: Self::S) -> Option<Self::P> {
        state.get_winner()
    }

    fn get_is_ended_from_state(state: &Self::S) -> bool {
        state.is_ended()
    }

    fn get_derivatives_from_state(state: &Self::S) -> Self::SI {
        let start = std::time::Instant::now();
        let mut state = state.clone();
        let res = state.get_derivatives();
        let end = std::time::Instant::now();
        res
    }

    fn get_last_action_from_state(state: &Self::S) -> Option<Self::A> {
        state.get_last_action()
    }

    fn get_nn_output_from_state(&mut self, state: &Self::S) -> (Vec<f32>, f32) {
        let me = state.get_next_player();
        let (state_tensor, drawn_tensor, scores_tensor) = state.as_tensor();
        {
            let mut op = self.model.operate();
            op.add_feed("predict", "state", &state_tensor);
            op.add_feed("predict", "drawn", &drawn_tensor);
            op.add_feed("predict", "cur_scores", &scores_tensor);
            let policy_out_token = op.request_fetch("predict", "output_0");
            let value_out_token = op.request_fetch("predict", "output_1");
            op.run().expect("Failed to run training");

            let policy_tensor: tf::Tensor<f32> = op
                .fetch(policy_out_token)
                .expect("Failed to fetch policy tensor");
            let value_tensor: tf::Tensor<f32> = op
                .fetch(value_out_token)
                .expect("Failed to fetch value tensor");
            let mut policy_vec = vec![0f32; 3584];
            for i in 0..3584 {
                policy_vec[i] = policy_tensor[i];
            }
            (policy_vec, value_tensor[0] as f32)
        }
    }

    fn map_action_to_policy_index(action: Self::A) -> usize {
        let (x, y) = action.get_location();
        let tile_idx = action.get_tile_idx();
        y * (16 * 14) + x * 14 + tile_idx
    }

    fn get_result(state: &Self::S, action: &Self::A) -> Self::S {
        state.get_state_after_action(*action)
    }
}

impl Agent for NNMCTSAgent {
    type S = ccs::State;

    type A = ccs::Action;

    fn query_action(&mut self, state: &Self::S) -> Self::A {
        self.query_mcts(state.clone(), self.playout_cap).0
    }
}
