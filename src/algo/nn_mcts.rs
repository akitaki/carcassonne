use indextree::Arena;
use ord_subset::OrdSubsetIterExt;
use rand::prelude::*;
use rand_distr::{Dirichlet, Distribution};
use std::rc::Rc;

#[derive(Debug)]
enum MaybeState<S, A> {
    State(Rc<S>),
    Action(Rc<S>, A),
}

impl<S, A> MaybeState<S, A> {
    fn from_state(state: S) -> Self {
        MaybeState::State(Rc::new(state))
    }

    fn from_parent_and_action(parent_state: Rc<S>, action: A) -> Self {
        MaybeState::Action(parent_state, action)
    }

    fn convert_to_state<F: FnOnce(&S, &A) -> S>(&mut self, state_action_to_state: F) {
        if let MaybeState::Action(parent_state, action) = self {
            *self = MaybeState::State(Rc::new(state_action_to_state(parent_state, action)));
        }
    }
}

#[derive(Debug)]
struct Node<S, A> {
    maybe_state: MaybeState<S, A>,
    visit_count: usize,
    cum_value: f32,
    nn_value: Option<f32>,
    policy_prior: f32,
}

impl<S, A> Node<S, A>
where
    A: Clone,
{
    fn from_state(state: S, policy_prior: f32) -> Self {
        Self {
            maybe_state: MaybeState::from_state(state),
            visit_count: 0,
            cum_value: 0f32,
            nn_value: None,
            policy_prior,
        }
    }

    fn from_parent_and_action(parent: Rc<S>, action: A, policy_prior: f32) -> Self {
        Self {
            maybe_state: MaybeState::from_parent_and_action(parent, action),
            visit_count: 0,
            cum_value: 0f32,
            nn_value: None,
            policy_prior,
        }
    }

    fn convert_to_state<F: FnOnce(&S, &A) -> S>(&mut self, state_action_to_state: F) {
        self.maybe_state.convert_to_state(state_action_to_state);
    }

    fn unwrap_state(&self) -> Rc<S> {
        if let MaybeState::State(state) = &self.maybe_state {
            state.clone()
        } else {
            panic!("Attempted to unwrap state on MaybeState::Action variant");
        }
    }

    fn try_get_action(&self) -> Option<A> {
        match &self.maybe_state {
            MaybeState::Action(_, action) => Some(action.clone()),
            _ => None,
        }
    }
}

pub trait NNMCTS {
    /// The state type, used to store a game state (including last move & next player information).
    type S: Clone;
    /// The action type, mainly used as return type of [`MCTS::query_mcts`].
    type A: Copy;
    /// The player type, used to determine next player, winner etc.
    type P: PartialEq + Copy;
    /// The "state iterator" type, used as the return type of [`MCTS::get_derivatives_from_state`].
    type SI: Iterator<Item = (Self::S, Self::A)>;

    fn get_next_player_from_state(state: Self::S) -> Self::P;
    fn get_winner_from_state(state: Self::S) -> Option<Self::P>;
    fn get_is_ended_from_state(state: &Self::S) -> bool;
    // fn get_is_winner_opponent_of(state: &Self::S, player: Self::P) -> bool;
    fn get_derivatives_from_state(state: &Self::S) -> Self::SI;
    fn get_last_action_from_state(state: &Self::S) -> Option<Self::A>;
    // fn get_random_derivative_from_state(state: &Self::S) -> Option<(Self::S, Self::A)>;
    fn get_nn_output_from_state(&mut self, state: &Self::S) -> (Vec<f32>, f32);
    fn map_action_to_policy_index(action: Self::A) -> usize;
    fn get_result(state: &Self::S, action: &Self::A) -> Self::S;

    /// Query the optimal action of type `A` from a state `S`.
    /// The goal is to maximize the win rate of `get_next_player_from_state(starting_from_state)`
    /// (see [`MCTS::get_next_player_from_state`])
    fn query_mcts(
        &mut self,
        starting_from_state: Self::S,
        playout_cap: usize,
    ) -> (Self::A, Vec<f32>) {
        let mut search_tree = Arena::new();
        let tree_root = search_tree.new_node(Node::from_state(starting_from_state, 0f32));

        for _ in 0..playout_cap {
            // Selection phase
            let mut cur_node = tree_root;
            // while cur_node has at least a child, proceed down
            while cur_node.children(&search_tree).next().is_some() {
                let parent_visit_count =
                    search_tree.get(cur_node).unwrap().get().visit_count as f32;
                // out of childrens of cur_node, choose the one with maximum UCB value,
                // and then assign as the new cur_node
                let legal_move_count = cur_node.children(&search_tree).count();

                let noise = if legal_move_count >= 2 {
                    let distr = Dirichlet::new_with_size(
                        16.0 * 16.0 * 0.03 / (legal_move_count as f32 + 0.000001),
                        legal_move_count,
                    )
                    .expect("Failed to create dirichlet noise");

                    Some(distr.sample(&mut thread_rng()))
                } else {
                    None
                };

                cur_node = cur_node
                    .children(&search_tree)
                    .enumerate()
                    .ord_subset_max_by_key(|&(index, child)| {
                        let child_node = search_tree.get(child).unwrap().get();
                        let visit_count = child_node.visit_count as f32;
                        let value = child_node.cum_value / (visit_count + 0.00001);
                        let prior = if let Some(noise) = &noise {
                            child_node.policy_prior * 0.75 + 0.25 * noise[index]
                        } else {
                            child_node.policy_prior
                        };
                        if visit_count != 0.0 {
                            value
                                + 2f32.sqrt()
                                    * prior
                                    * (parent_visit_count.ln() / (1. + visit_count)).sqrt()
                        } else {
                            f32::MAX
                        }
                    })
                    .map(|(_index, child)| child)
                    .unwrap();
            }

            // Expansion phase
            // if cur_node's state is non-ending state
            if cur_node == tree_root || search_tree.get(cur_node).unwrap().get().visit_count > 3 {
                let leaf_node = search_tree.get_mut(cur_node).unwrap().get_mut();
                leaf_node.convert_to_state(|state, action| Self::get_result(state, action));

                let leaf_node_state_is_ended =
                    Self::get_is_ended_from_state(leaf_node.unwrap_state().as_ref());

                // TODO: Delete this comment
                // if cur_node == tree_root {
                // info!(
                // "Tree root expansion case.\nleaf_node_is_ended: {}",
                // leaf_node_state_is_ended
                // );
                // }

                if !leaf_node_state_is_ended {
                    // clone cur_node's state
                    let leaf_state = leaf_node.unwrap_state();

                    // find all derived states from cur_node's state, and then append to
                    // cur_node
                    let (policy_priors, _parent_value) =
                        self.get_nn_output_from_state(leaf_state.as_ref());

                    // Create all child nodes (while making them unexpanded)
                    for (derived_state, deriving_action) in
                        Self::get_derivatives_from_state(leaf_state.as_ref())
                    {
                        let policy_index = Self::map_action_to_policy_index(deriving_action);
                        let policy_prior = policy_priors[policy_index];
                        let derived_nodeid = search_tree.new_node(Node::from_parent_and_action(
                            leaf_state.clone(),
                            deriving_action,
                            policy_prior,
                        ));
                        cur_node.append(derived_nodeid, &mut search_tree);
                    }

                    // move down from cur_node to a randomly chosen child
                    if cur_node.children(&search_tree).next().is_some() {
                        cur_node = cur_node
                            .children(&search_tree)
                            .choose(&mut thread_rng())
                            .unwrap();
                    }
                }
            }

            // Backprop
            let bottom_node = search_tree.get_mut(cur_node).unwrap().get_mut();
            bottom_node.convert_to_state(|state, action| Self::get_result(state, action));
            if let None = bottom_node.nn_value {
                let (_policies, bottom_node_value) =
                    self.get_nn_output_from_state(bottom_node.unwrap_state().as_ref());
                bottom_node.nn_value = Some(bottom_node_value);
            }
            let score = bottom_node.nn_value.unwrap();

            let mut cur_node_opt = Some(cur_node);
            // while cur_node_opt is not None
            while let Some(cur_node) = cur_node_opt {
                {
                    let cur_node = search_tree.get_mut(cur_node).unwrap().get_mut();
                    cur_node.visit_count += 1;
                    cur_node.cum_value += score;
                }
                // set cur_node_opt to cur_node's parent if it exists
                // otherwise None is given (effectively terminating the loop)
                if let Some(parent_node) = search_tree.get(cur_node).unwrap().parent() {
                    cur_node_opt = Some(parent_node);
                } else {
                    cur_node_opt = None;
                }
            }

            if search_tree.count() > 200000 {
                warn!("Search tree contains {} nodes", search_tree.count());
            }
        }

        let best_child_node_id = tree_root
            .children(&search_tree)
            .max_by_key(|&child_id| search_tree.get(child_id).unwrap().get().visit_count)
            .unwrap_or(tree_root.children(&search_tree).next().unwrap());

        /* Pi distribution calculation */
        let mut pi_dist: Vec<f32> = vec![0.0; 16 * 16 * (1 + 4 + 4 + 4 + 1)];

        // For each node of the root node
        let children_of_root: Vec<_> = tree_root.children(&search_tree).collect();
        for child_node_id in children_of_root.into_iter() {
            let child_node = search_tree.get_mut(child_node_id).unwrap().get_mut();

            // Convert the node from action node to state node
            child_node.convert_to_state(|state, action| Self::get_result(state, action));
            let child_action = Self::get_last_action_from_state(child_node.unwrap_state().as_ref());

            // If it has an action (it should always have!), set its value to number of visits
            if let Some(action) = child_action {
                let policy_index = Self::map_action_to_policy_index(action);
                pi_dist[policy_index] = child_node.visit_count as f32;
            }
        }
        let pi_sum: f32 = pi_dist.iter().sum();
        for p in pi_dist.iter_mut() {
            *p /= pi_sum + 0.00000001;
        }

        let ref best_child_state = search_tree
            .get(best_child_node_id)
            .unwrap()
            .get()
            .unwrap_state();
        let best_child_last_action = Self::get_last_action_from_state(best_child_state).unwrap();
        (best_child_last_action, pi_dist)
    }
}
