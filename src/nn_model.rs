use std::cell::Cell;
use std::collections::HashMap;
use std::io::{Read, Write};
use tensorflow as tf;

pub struct Model {
    graph: tf::Graph,
    session: tf::Session,
    signatures: HashMap<String, SignatureDef>,
    save_op_name: String,
}

impl Model {
    /// Create new model from a SavedModel bundle
    pub fn new_from_saved_model_path<P: AsRef<str>>(path: P) -> Result<Self, Error> {
        let mut graph = tf::Graph::new();
        let bundle = tf::SavedModelBundle::load(
            &tf::SessionOptions::new(),
            &["serve"],
            &mut graph,
            path.as_ref(),
        )?;
        let signatures: HashMap<String, SignatureDef> = bundle
            .meta_graph_def()
            .signatures()
            .iter()
            .map(|(k, v)| (k.clone(), v.into()))
            .collect();
        let tf::SavedModelBundle { session, .. } = bundle;

        let outnames: std::collections::HashSet<_> = signatures
            .iter()
            .map(|(_, signature_def)| signature_def.outputs.iter().map(|(_, v)| v))
            .flatten()
            .collect();

        // Get names of "StatefulPartitionedCall_<NUMBER>" operations
        let save_op_name = graph
            .operation_iter()
            .filter_map(|op| {
                op.name()
                    .ok()
                    .filter(|name| name.starts_with("StatefulPartitionedCall_"))
            })
            .filter(|name| !outnames.contains(name))
            .min_by_key(|name| {
                name.get("StatefulPartitionedCall_".len()..)
                    .unwrap()
                    .parse::<usize>()
                    .unwrap_or(usize::MAX)
            })
            .ok_or(Error::ModelIOError(
                "Failed to get saver operation name from saved model bundle".to_string(),
            ))?;
        info!(
            "Imported model bundle with saver operation name `{}`",
            save_op_name
        );
        // let save_op_name = Cell::new(save_op_name);

        Ok(Self {
            graph,
            session,
            signatures,
            save_op_name,
        })
    }

    /// Save states back to the original SavedBundle
    pub fn save_model_to_path<P: AsRef<str>>(&mut self, path: P) -> Result<(), Error> {
        info!("Saving model...");
        // Get the full path to "<model>/variables/variables" as a string tensor
        let vars_filename = std::fs::canonicalize(path.as_ref())
            .map_err(|_| {
                Error::ModelIOError("Failed to construct absolute path to <model>".to_string())
            })?
            .join("variables")
            .join("variables")
            .to_str()
            .ok_or(Error::ModelIOError(
                "Failed to convert pathbuf to UTF-8".to_string(),
            ))?
            .to_string();
        info!("Variables file path: {:?}", vars_filename);
        let vars_filename = tf::Tensor::from(vars_filename);

        let save_op_name = self.save_op_name.clone();

        {
            let mut op = self.operate();
            let saver_filename_op = op
                .model
                .graph
                .operation_by_name_required("saver_filename")?;
            let save_op = op.model.graph.operation_by_name_required(&save_op_name)?;
            op.run_args.add_feed(&saver_filename_op, 0, &vars_filename);
            op.run_args.add_target(&save_op);
            op.run()?;
        }

        // self.save_op_name.set(save_op_name);
        info!("Saved model!");

        Ok(())
    }

    pub fn operate(&mut self) -> ModelOperator<'_> {
        ModelOperator {
            model: self,
            run_args: tf::SessionRunArgs::new(),
        }
    }

    fn debug_print(&mut self) {
        let save_op_name = self.save_op_name.clone();
        println!("SAVE_OP_NAME\n{:#?}", save_op_name);
        // self.save_op_name.set(save_op_name);
        println!("SIGNATURES\n{:#?}", self.signatures);
        println!(
            "OPERATORS\n{:#?}",
            self.graph
                .operation_iter()
                .map(|op| { (op.name(), op.op_type(), op.num_inputs(), op.num_outputs()) })
                .collect::<Vec<_>>()
        );
    }
}

impl Drop for Model {
    fn drop(&mut self) {
        self.session.close().unwrap();
    }
}

#[derive(Debug, Clone)]
struct SignatureDef {
    inputs: HashMap<String, String>,
    outputs: HashMap<String, String>,
}

impl From<&tf::SignatureDef> for SignatureDef {
    fn from(def: &tf::SignatureDef) -> Self {
        Self {
            inputs: def
                .inputs()
                .iter()
                .map(|(k, v)| (k.clone(), v.name().name.clone()))
                .collect(),
            outputs: def
                .outputs()
                .iter()
                .map(|(k, v)| (k.clone(), v.name().name.clone()))
                .collect(),
        }
    }
}

/// Supplementary type to perform operations on a model
pub struct ModelOperator<'a> {
    model: &'a mut Model,
    run_args: tf::SessionRunArgs<'a>,
}

#[derive(Debug)]
enum ModelOpType {
    In,
    Out,
}

impl<'a> ModelOperator<'a> {
    fn get_op_from(&self, sig_name: &str, io_name: &str, op_type: ModelOpType) -> tf::Operation {
        let ref signature = self.model.signatures[sig_name];
        let ios = match op_type {
            ModelOpType::In => &signature.inputs,
            ModelOpType::Out => &signature.outputs,
        };
        let ref op_name = ios[io_name];
        let op = self
            .model
            .graph
            .operation_by_name_required(op_name)
            .expect(&format!(
                "Failed to get input/output/target '{}' from signature '{}'",
                sig_name, io_name
            ));
        op
    }

    pub fn add_feed<T: tf::TensorType, U: AsRef<str>, V: AsRef<str>>(
        &mut self,
        sig_name: U,
        input_name: V,
        tensor: &'a tf::Tensor<T>,
    ) {
        let op = self.get_op_from(sig_name.as_ref(), input_name.as_ref(), ModelOpType::In);
        self.run_args.add_feed(&op, 0, tensor);
    }

    pub fn add_target<U: AsRef<str>, V: AsRef<str>>(&mut self, sig_name: U, target_name: V) {
        let op = self.get_op_from(sig_name.as_ref(), target_name.as_ref(), ModelOpType::Out);
        self.run_args.add_target(&op);
    }

    pub fn request_fetch<U: AsRef<str>, V: AsRef<str>>(
        &mut self,
        sig_name: U,
        output_name: V,
    ) -> tf::FetchToken {
        let op = self.get_op_from(sig_name.as_ref(), output_name.as_ref(), ModelOpType::Out);
        self.run_args.request_fetch(&op, 0)
    }

    pub fn run(&mut self) -> Result<(), Error> {
        Ok(self.model.session.run(&mut self.run_args)?)
    }

    pub fn fetch<T: tf::TensorType>(
        &mut self,
        token: tf::FetchToken,
    ) -> Result<tf::Tensor<T>, Error> {
        Ok(self.run_args.fetch(token)?)
    }
}

#[derive(Debug)]
pub enum Error {
    TFError(tf::Status),
    IOError(std::io::Error),
    Utf8Error(core::str::Utf8Error),
    ModelIOError(String),
}

impl From<tf::Status> for Error {
    fn from(e: tf::Status) -> Self {
        Self::TFError(e)
    }
}

impl From<std::io::Error> for Error {
    fn from(e: std::io::Error) -> Self {
        Self::IOError(e)
    }
}

impl From<core::str::Utf8Error> for Error {
    fn from(e: core::str::Utf8Error) -> Self {
        Self::Utf8Error(e)
    }
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(f, "{:?}", self)
    }
}

impl std::error::Error for Error {}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn model_save_load_test() -> Result<(), Box<dyn std::error::Error>> {
        let mut model = Model::new_from_saved_model_path("test_model")?;

        let first_value = {
            let mut op = model.operate();
            let inc_out_token = op.request_fetch("inc", "output_0");
            op.run()?;
            let value: f32 = op.fetch(inc_out_token)?[0];
            value
        };
        model.save_model_to_path("test_model")?;

        let second_value = {
            let mut op = model.operate();
            let inc_out_token = op.request_fetch("inc", "output_0");
            op.run()?;
            let value: f32 = op.fetch(inc_out_token)?[0];
            value
        };
        model.save_model_to_path("test_model")?;

        println!("First and second value: {}, {}", first_value, second_value);

        assert_eq!(second_value - first_value, 1.);

        Ok(())
    }

    #[test]
    fn main_model_test() -> Result<(), Box<dyn std::error::Error>> {
        let mut model = Model::new_from_saved_model_path("model")?;
        model.debug_print();
        Ok(())
    }
}
